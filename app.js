// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);


// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form POST data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));


// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));
console.log(__dirname);

// Specify that when we browse to "/" with a GET request, render the
// questionnaire/questionnaire-form view
app.get('/', function (req, res) {

    res.redirect(301, '/home');
});

app.get('/home', function (req, res) {

    res.render("configui/home");
});

// Specify that when we browse to "/getrecord" with a GET request, render the
app.get('/getrecord', function (req, res) {

    res.render("configui/sample-form", { pageTitle: "Ex 03 Form", useGet: true });
});

app.get('/removerecord', function (req, res) {

    res.render("configui/sample-form", { pageTitle: "Ex 03 Form", useGet: true });
});


app.get('/createdatapool', function (req, res) {
    console.log("Inside create datapool");

    res.render("configui/sample-form", { pageTitle: "Ex 03 Form", useGet: true });
});

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});